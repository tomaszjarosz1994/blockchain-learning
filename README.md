How to run solidity application.

1. Install npm (my version 9.1.3)
2. Install node (my version v18.10.0)
3. Install Ganache
4. Run ganache (default settings)
5. Add Ganache network to metamask
6. Export first ganache address to metamask
7. Install project ``npm install`` 
8. Deploy smart contracts ``truffle migrate --reset``
9. Run application: ``npm start ``
10. Application is up and running``http://localhost:3000/``